﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParallelStopWatch
{
    static class ButtonImages
    {
        private static string play = "/Assets/AppBar/play.png";
        private static string pause = "/Assets/AppBar/pause.png";
        private static string lap = "/Assets/AppBar/lap.png";
        private static string open = "/Assets/AppBar/open.png";
        private static string close = "/Assets/AppBar/close.png";

        public static void Init(int Theme)
        {
            // 1 - Dark / 0 - Light
            const int Light = 0;
            if (Theme == Light)
            {
                play = "/Assets/AppBar/play.black.png";
                pause = "/Assets/AppBar/pause.black.png";
                lap = "/Assets/AppBar/lap.black.png";
                open = "/Assets/AppBar/open.black.png";
                close = "/Assets/AppBar/close.black.png";
            }
        }

        public static string GetPlay()
        {
            return play;
        }

        public static string GetPause()
        {
            return pause;
        }

        public static string GetLap()
        {
            return lap;
        }

        public static string GetOpen()
        {
            return open;
        }

        public static string GetClose()
        {
            return close;
        }
    }
}
