﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34003
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ParallelStopWatch.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AppResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AppResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ParallelStopWatch.Resources.AppResources", typeof(AppResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to about.
        /// </summary>
        public static string AboutPageNameText {
            get {
                return ResourceManager.GetString("AboutPageNameText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to add.
        /// </summary>
        public static string AddBarButtonText {
            get {
                return ResourceManager.GetString("AddBarButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Menu Item.
        /// </summary>
        public static string AppBarMenuItemText {
            get {
                return ResourceManager.GetString("AppBarMenuItemText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parallel Stopwatch.
        /// </summary>
        public static string ApplicationTitle {
            get {
                return ResourceManager.GetString("ApplicationTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Author: Artem Ustimov.
        /// </summary>
        public static string AuthorText {
            get {
                return ResourceManager.GetString("AuthorText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact:.
        /// </summary>
        public static string ContactText {
            get {
                return ResourceManager.GetString("ContactText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Continue?.
        /// </summary>
        public static string ContinueText {
            get {
                return ResourceManager.GetString("ContinueText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to will be removed all stopwatches.
        /// </summary>
        public static string DelAllStopWatches {
            get {
                return ResourceManager.GetString("DelAllStopWatches", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to remove all.
        /// </summary>
        public static string DelBarButtonText {
            get {
                return ResourceManager.GetString("DelBarButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to will be removed this stopwatch.
        /// </summary>
        public static string DelStopWatchText {
            get {
                return ResourceManager.GetString("DelStopWatchText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Press &quot;play&quot; button to start stopwatch and &quot;pause&quot; button to stop stopwatch. Press &quot;add&quot; button to create new stopwatch. You can create unlimited number of stopwatches. Hold stopwatch for deleting. Press &quot;L&quot; button to get time of lap. Button with arrow opens / closes list of laps. Press &quot;remove all&quot; button for deleting all stopwatches. To refresh stopwatch you can simply remove this stopwatch and create new..
        /// </summary>
        public static string HelpingText {
            get {
                return ResourceManager.GetString("HelpingText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to help.
        /// </summary>
        public static string HelpText {
            get {
                return ResourceManager.GetString("HelpText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lap.
        /// </summary>
        public static string LapText {
            get {
                return ResourceManager.GetString("LapText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Release date: 04.01.14.
        /// </summary>
        public static string ReleaseDateText {
            get {
                return ResourceManager.GetString("ReleaseDateText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to LeftToRight.
        /// </summary>
        public static string ResourceFlowDirection {
            get {
                return ResourceManager.GetString("ResourceFlowDirection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to en-US.
        /// </summary>
        public static string ResourceLanguage {
            get {
                return ResourceManager.GetString("ResourceLanguage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Version: 0.1.1.
        /// </summary>
        public static string VersionText {
            get {
                return ResourceManager.GetString("VersionText", resourceCulture);
            }
        }
    }
}
