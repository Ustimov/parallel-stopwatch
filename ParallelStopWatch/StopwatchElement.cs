﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Diagnostics;
using System.Windows;
using System.Windows.Navigation;
using System.Windows.Media.Imaging;
using ParallelStopWatch.Resources;

namespace ParallelStopWatch
{
    class StopwatchElement : StackPanel
    {
        private Stopwatch libraryStopwatch = new Stopwatch();
        private StackPanel laps = new StackPanel();
        private StackPanel stopwatch = new StackPanel();
       
        private TextBlock dial;
        private Image expanderButton;
        private Image startButton;

        private static int iCount = 0;
        private int iLap = 0;
        
        public StopwatchElement() : base()
        {
            iCount++;
            stopwatch.Orientation = Orientation.Horizontal;

            // Stopwatch init (order is important)
            AddStopwatchNumber();
            AddStopwatchDial();
            AddStopwatchLapButton();
            AddStopwatchStartButton();
            AddStopwatchExpanderButton();

            // Stopwatch and laps container
            this.Children.Add(stopwatch);
            this.Children.Add(laps);
        }
        
        public static void ResetCount()
        {
            iCount = 0;
        }

        public void Update()
        {
            dial.Text = libraryStopwatch.Elapsed.ToString().Substring(1, 10);
        }

        public bool IsRunning()
        {
            return libraryStopwatch.IsRunning;
        }
        
        private void AddStopwatchNumber()
        {
            var number = new TextBlock
            {
                TextAlignment = System.Windows.TextAlignment.Right,
                Text = String.Format("{0}. ", iCount),
                Width = 70,
                FontSize = 42,
            };
            stopwatch.Children.Add(number);
        }

        private void AddStopwatchDial()
        {
            dial = new TextBlock
            {
                Text = "0:00:00.00",
                Width = 200,
                FontSize = 42,
            };
            dial.Hold += new EventHandler<System.Windows.Input.GestureEventArgs>(DeleteStopwatch);
            stopwatch.Children.Add(dial);
        }
        
        private void AddStopwatchLapButton()
        {
            var lapButton = new Image
            {
                Source = new BitmapImage(new Uri(ButtonImages.GetLap(), UriKind.Relative)),
                MaxHeight = 58,
            };
            lapButton.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(LapButton_Click);
            stopwatch.Children.Add(lapButton);
        }
         
        private void AddStopwatchStartButton()
        {
            startButton = new Image()
            {
                Source = new BitmapImage(new Uri(ButtonImages.GetPlay(), UriKind.Relative)),
                MaxHeight = 58,
            };
            startButton.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(StartButton_Click);
            stopwatch.Children.Add(startButton);
        }
        
        private void AddStopwatchExpanderButton()
        {
            expanderButton = new Image
            {
                Source = new BitmapImage(new Uri(ButtonImages.GetClose(), UriKind.Relative)),
                MaxHeight = 58,
            };
            expanderButton.Tap += new EventHandler<System.Windows.Input.GestureEventArgs>(ExpanderButton_Click);
            stopwatch.Children.Add(expanderButton);
        }

        // Laps expander
        private void ExpanderButton_Click(object sender, RoutedEventArgs e)
        {
            if (laps.Visibility == Visibility.Visible)
            {
                laps.Visibility = Visibility.Collapsed;
                expanderButton.Source = new BitmapImage(new Uri(ButtonImages.GetOpen(), UriKind.Relative));
            }
            else
            {
                laps.Visibility = Visibility.Visible;
                expanderButton.Source = new BitmapImage(new Uri(ButtonImages.GetClose(), UriKind.Relative));
            }
        }

        // Delete choicen stopwatch
        private void DeleteStopwatch(object sender, EventArgs e)
        {
            
            if (MessageBox.Show(AppResources.DelStopWatchText, AppResources.ContinueText, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                this.Children.Clear();
                this.Visibility = Visibility.Collapsed;
                libraryStopwatch.Stop();
            }
        }
        
        // For popup window
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            this.Children.Clear();
            this.Visibility = Visibility.Collapsed;

        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            // Start / Pause switch
            if (libraryStopwatch.IsRunning == false)
            {
                libraryStopwatch.Start();
                startButton.Source = new BitmapImage(new Uri(ButtonImages.GetPause(), UriKind.Relative));
            }
            else
            {
                libraryStopwatch.Stop();
                startButton.Source = new BitmapImage(new Uri(ButtonImages.GetPlay(), UriKind.Relative));
            }
        }

        // Adding new lap
        private void LapButton_Click(object sender, EventArgs e)
        {
            //No laps then stopwatch is disabled
            if (libraryStopwatch.IsRunning == false)
                return;

            iLap++;
            var newLap = new TextBlock
            {
                Text = String.Format("{0} {1}: ", AppResources.LapText, iLap) + libraryStopwatch.Elapsed.ToString().Substring(1, 10),
                TextAlignment = TextAlignment.Center,
            };
            laps.Children.Add(newLap);
        }
    }
}