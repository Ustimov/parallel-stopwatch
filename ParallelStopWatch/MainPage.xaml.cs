﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ParallelStopWatch.Resources;
using System.Diagnostics;
using System.Windows.Threading;
using System.Windows.Media.Imaging;

namespace ParallelStopWatch
{
    public partial class MainPage : PhoneApplicationPage
    {
        // For tick event
        DispatcherTimer ticker = new DispatcherTimer();
        UIElementCollection initializedStopwatchElements;

        public MainPage()
        {
            InitializeComponent();

            int Theme = (int)(double)Resources["PhoneDarkThemeOpacity"];
            ButtonImages.Init(Theme);

            InitTicker();
            
            BuildLocalizedApplicationBar();

            // Create first stopwatch on start
            initializedStopwatchElements = stopwatchPanel.Children;
            initializedStopwatchElements.Add(new StopwatchElement());
        }
     
        private void BuildLocalizedApplicationBar()
        {
            ApplicationBar = new ApplicationBar();
            
            var addStopwatchButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/add.png", UriKind.Relative));
            var deleteAllStopwatchesButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/delete.png", UriKind.Relative));
            
            addStopwatchButton.Text = AppResources.AddBarButtonText;
            deleteAllStopwatchesButton.Text = AppResources.DelBarButtonText;
                        
            ApplicationBar.Buttons.Add(addStopwatchButton);
            ApplicationBar.Buttons.Add(deleteAllStopwatchesButton);

            addStopwatchButton.Click += new EventHandler(AddStopwatchElement);
            deleteAllStopwatchesButton.Click += new EventHandler(DeleteAllStopwathes);

            var aboutBarMenuItem = new ApplicationBarMenuItem(AppResources.AboutPageNameText);
            var helpBarMenuItem = new ApplicationBarMenuItem(AppResources.HelpText);
            
            aboutBarMenuItem.Click += new EventHandler(OpenAboutPage);
            helpBarMenuItem.Click += new EventHandler(OpenHelpPage);
            
            ApplicationBar.MenuItems.Add(helpBarMenuItem);
            ApplicationBar.MenuItems.Add(aboutBarMenuItem);
            
        }
        private void OpenAboutPage(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }
        
        private void OpenHelpPage(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/HelpPage.xaml", UriKind.Relative));
        }

        private void InitTicker()
        {
            ticker.Tick += new EventHandler(tickerTick);
            ticker.Interval = TimeSpan.FromMilliseconds(100); // Save proccessor time
            ticker.Start();
        }

        private void DeleteAllStopwathes(object sender, EventArgs e)
        {
            if (MessageBox.Show(AppResources.DelAllStopWatches, AppResources.ContinueText, MessageBoxButton.OKCancel) == MessageBoxResult.OK)
            {
                initializedStopwatchElements.Clear();
                StopwatchElement.ResetCount();
            }
        }

        private void tickerTick(object sender, EventArgs e)
        {
            foreach (StopwatchElement stopwatchElement in initializedStopwatchElements)
            {
                if (stopwatchElement.IsRunning() == true) // && (stopwatch.Children.Count != 0)) //For working substring method
                {
                    stopwatchElement.Update();
                }
            }

            //Deleting objects
            List<StopwatchElement> fordel = new List<StopwatchElement>();
            foreach (StopwatchElement stopwatchElement in initializedStopwatchElements)
            {
                if (stopwatchElement.Children.Count == 0)
                {
                    fordel.Add(stopwatchElement);
                }
            }
         
            foreach (StopwatchElement stopwatchElement in fordel)
            {
                initializedStopwatchElements.Remove(stopwatchElement);
            }
        }

        private void AddStopwatchElement(object sender, EventArgs e)
        {
            initializedStopwatchElements.Add(new StopwatchElement());
        }
    }
}