# README #

### Description ###

* This app for Windows Phone 8 allows to create a lot of stopwatches (unlimited) in one window. Every stopwatch can be started / stopped / deleted separetely. Also every stopwatch can show time of laps.

### Version ###

* This version is 0.1.1.

### Changelog ###

* Date: 29.12.13
* Version: 0.1
- First Release
#
* Date: 04.01.14
* Version: 0.1.1
- Fix: Added support light theme of phone (invisible buttons fixed)

### Windows Phone Store ###
* You can try it or watch screenshots on Windows Phone Store:
http://www.windowsphone.com/en-Us/store/app/parallel-stopwatch/a542572d-403b-449f-9009-67e02fa9a155

### Other ###

* Project of Visual Studio 2013 Professional
* This is final vesion
* Visit my web site: http://ustimov.org